PANDOC=pandoc

BASEDIR=$(CURDIR)
INPUTDIR=$(BASEDIR)/md
OUTPUTDIR=$(BASEDIR)/output
STYLEDIR=$(BASEDIR)/style
DOCCLASS=report
DOCNAME=Economic_StylizedFacts
BIBFILE=$(INPUTDIR)/Z_References.bib

pdf:
	pandoc "$(INPUTDIR)"/*.md \
	-o "$(OUTPUTDIR)/$(DOCNAME).pdf" \
	-H "$(STYLEDIR)/preamble.tex" \
	--template="$(STYLEDIR)/template.tex" \
	--bibliography="$(BIBFILE)" 2>pandoc.log \
	--csl="$(STYLEDIR)/ref_format.csl" \
	--highlight-style pygments \
	--filter pandoc-crossref \
	-V toc-depth=2 \
	-V fontsize=11pt \
	-V papersize=a4paper \
	-V documentclass=$(DOCCLASS) \
	-V classoption=twoside \
	-N \
	--pdf-engine=xelatex \

##--verbose


tex:
	pandoc "$(INPUTDIR)"/*.md \
	-o "$(OUTPUTDIR)/$(DOCNAME).tex" \
	-H "$(STYLEDIR)/preamble.tex" \
	--template="$(STYLEDIR)/template.tex" \
	--bibliography="$(BIBFILE)" \
	--csl="$(STYLEDIR)/ref_format.csl" \
	--highlight-style pygments \
	--filter pandoc-crossref \
	-V toc-depth=2 \
	-V fontsize=12pt \
	-V papersize=a4paper \
	-V documentclass=$(DOCCLASS) \
  -V twoside \
	-N \
	--verbose


tuftepdf:
	pandoc "$(INPUTDIR)"/*.md \
	-o "$(OUTPUTDIR)/$(DOCNAME)_tufte.pdf" \
	-H "$(STYLEDIR)/preamble.tex" \
	--template="$(STYLEDIR)/tufte_template.tex" \
	--bibliography="$(BIBFILE)" 2>pandoc.log \
	--csl="$(STYLEDIR)/ref_format.csl" \
	--highlight-style pygments \
	--filter pandoc-crossref \
	-V fontsize=12pt \
	-V papersize=a4paper \
	-V classoption=twoside \
	-V documentclass=tufte-handout \
	-N \
	--pdf-engine=xelatex

# --verbose

# --latex-engine=xelatex \
# tintpdf:
# 	pandoc "$(INPUTDIR)"/*.md \
# 	-o "$(OUTPUTDIR)/MAThesis_ReiterO_tint.pdf" \
# 	-H "$(STYLEDIR)/preamble.tex" \
# 	--template="$(STYLEDIR)/tint_template.tex" \
# 	--bibliography="$(BIBFILE)" 2>pandoc.log \
# 	--csl="$(STYLEDIR)/ref_format.csl" \
# 	--highlight-style pygments \
# 	-V fontsize=12pt \
# 	-V papersize=a4paper \
# 	-V documentclass=$(DOCCLASS) \
# 	-N \
# 	--pdf-engine=xelatex \
# 	--verbose

# html:
# 	pandoc "$(INPUTDIR)"/*.md \
# 	-o "$(OUTPUTDIR)/thesis.html" \
# 	--standalone \
# 	--template="$(STYLEDIR)/template.html" \
# 	--bibliography="$(BIBFILE)" \
# 	--csl="$(STYLEDIR)/ref_format.csl" \
# 	--include-in-header="$(STYLEDIR)/style.css" \
# 	--toc \
# 	--number-sections
# 	rm -rf "$(OUTPUTDIR)/source"
# 	mkdir "$(OUTPUTDIR)/source"
# 	cp -r "$(INPUTDIR)/figures" "$(OUTPUTDIR)/source/figures"

.PHONY: tex pdf
