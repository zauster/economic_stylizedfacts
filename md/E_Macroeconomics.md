
# Macroeconomics

## Kaldor's facts about economic growth

From @kaldor1957model:

- The shares of national income received by labor and capital are
  roughly constant over long periods of time
- The rate of growth of the capital stock per worker is roughly
  constant over long periods of time
- The rate of growth of output per worker is roughly constant
  over long periods of time
- The capital/output ratio is roughly constant over long periods
  of time
- The rate of return on investment is roughly constant over long
  periods of time
- There are appreciable variations (2 to 5 percent) in the rate
  of growth of labor productivity and of total output among
  countries.

## Cyclicality

See @uribe2017open.

### Pro-cyclical

- Consumption is positively correlated with GDP
- Exports are positively correlated with GDP
- Imports are positively correlated with GDP

### Counter-cyclical

- Unemployment is negatively correlated with GDP

## Volatility

- Consumption is less volatile than GDP
- Exports, imports and unemployment are more volatile than GDP
- Variance of production is larger than the variance of sales

## Inflation

See @bernard1999exceptional, @bernard2003plants,
@bernard2007firms and @degregorio1994international.

- Inflation in domestic goods is higher than in internationally
  traded goods
- Labour productivity growth in domestic goods is lower than in
  internationally traded goods
