
# README

This repository contains a collection of economic stylized facts.

## Structure

- `md`: The markdown-files are contained in the `md` directory
and named after the JEL codes.
- `output`: This directory will hold the generated PDFs.
- `styles`: Files that are needed to produce the PDF can be found here.

## Contributing

Yes, please!

Stylized facts and their references are always welcome!
